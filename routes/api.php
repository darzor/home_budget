<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::namespace('\App\Http\Controllers')->group( function () {

    Route::post('register', 'LoginController@register');
    Route::post('login', 'LoginController@login' );

    Route::middleware('auth:api' )->group( function( ) {

        Route::get( 'expenses/yearly', 'ExpensesController@yearly' );
        Route::get( 'expenses/quarterly', 'ExpensesController@quarterly' );
        Route::get( 'expenses/monthly', 'ExpensesController@monthly' );
        Route::get( 'expenses/weekly', 'ExpensesController@weekly' );
        Route::apiResource( 'expenses', 'ExpensesController' );
        Route::apiResource( 'categories', 'CategoriesController' );

        Route::get('logout', 'LoginController@logout' );

    } );

} );



