<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;

    protected $table = 'expenses';
    protected $primaryKey = 'id';
    public $timestamps = true;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];

    public function user(){
        return $this->belongsTo( User::class, 'user_id' );
    }

    public function category(){
        return $this->belongsTo( Category::class, 'category_id' );
    }

}
