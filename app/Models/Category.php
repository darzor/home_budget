<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public $fillable = [];
    public $guarded = [];
    public $hidden = [];


    public function expenses(){
        return $this->hasMany( Expense::class, 'category_id' );
    }
}
