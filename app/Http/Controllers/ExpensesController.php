<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Http\Requests\ExpenseRequest;
use App\Models\Category;
use App\Models\Expense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ExpensesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index( Request $request )
    {
        $expenses = Expense::with([
            'user',
            'category'
        ])
        ->where( function ( $q ) use ( $request ) {

            $q->where( 'user_id', auth()->user()->id );

            $q->when( $request->id, function ( $q, $id ) {
                return $q->where( 'id', $id );
            } );

            $q->when( $request->category_id, function ( $q, $category_id ) {
                return $q->where( 'category_id', $category_id );
            } );

            $q->when( $request->price_from, function ( $q, $price_from ) {
                return $q->where( 'price', '>=', $price_from );
            } );

            $q->when( $request->price_to, function ( $q, $price_to ) {
                return $q->where( 'price', '<=', $price_to );
            } );

            $q->when( $request->date, function ( $q, $date ) {
                return $q->where( 'date', $date );
            } );

            $q->when( $request->date_from, function ( $q, $date_from ) {
                return $q->where( 'date', '>=', $date_from );
            } );

            $q->when( $request->date_to, function ( $q, $date_to ) {
                return $q->where( 'date', '<=', $date_to );
            } );

            $q->when( $request->note, function ( $q, $note ) {
                return $q->where( 'note', 'like', '%' . $note . '%' );
            } );

        } )
        ->whereHas( 'category', function ( $q ) use ( $request ) {

            $q->when( $request->category_name, function ( $q, $category_name ) {
                return $q->where( 'name', 'like', '%' . $category_name . '%' );
            } );

        } )
        ->get();

        return $this->sendResponse( $expenses );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ExpenseRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ExpenseRequest $request)
    {
        $user = auth()->user();
        $expense = Expense::create(
            array_merge(
                $request->all(),
                [
                    'user_id' => $user->id
                ]
            )
        );
        $user->amount -= $expense->price;
        $user->save();

        return $this->show( $expense->id );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( $id )
    {
        $expenses = Expense::with(['category', 'user'])
            ->where( 'user_id', auth()->user()->id )
            ->find( $id );

        if( $expenses )
            return $this->sendResponse( $expenses );
        else
            return $this->sendError( 'Not found' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ExpenseRequest  $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( ExpenseRequest $request, $id )
    {
        $user = auth()->user();
        $expense = Expense::find( $id );

        if( $expense ){

            $user->amount += $expense->price;
            $user->save();
            $expense->update( $request->all() );
            $user->amount -= $expense->price;
            $user->save();

            return $this->show( $expense->id );
        } else {
            return $this->sendError( 'Not found' );
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy( $id )
    {
        $user = auth()->user();
        $expense = Expense::find( $id );

        if( $expense ){
            $user->amaunt += $expense->price;
            $user->save();
            return $this->sendResponse( [] );
        } else {
            return $this->sendError( 'Not found' );
        }

    }


    public function yearly(){
        $results = auth()->user()->expenses()
            ->select(
                DB::raw( 'sum(price) as sum' ),
                DB::raw("(DATE_FORMAT(date, '%Y' ) ) as date")
            )
            ->orderBy( 'date' )
            ->groupBy(DB::raw("DATE_FORMAT(date, '%Y')"))
            ->get();

        return $this->sendResponse( $results );
    }



    public function monthly(){
        $results = auth()->user()->expenses()
            ->select(
                DB::raw( 'sum(price) as sum' ),
                DB::raw("(DATE_FORMAT(date, '%Y-%m' ) ) as date")
            )
            ->orderBy( 'date' )
            ->groupBy(DB::raw("DATE_FORMAT(date, '%Y-%m')"))
            ->get();

        return $this->sendResponse( $results );
    }



    public function quarterly(){
        $results = auth()->user()->expenses()
            ->select(
                DB::raw( 'sum(price) as sum' ),
                DB::raw("CONCAT(YEAR(date), '/', QUARTER(date)) as date ")
            )
            ->orderBy( 'date' )
            ->groupBy(DB::raw("CONCAT(YEAR(date), '/', QUARTER(date))") )
            ->get();

        return $this->sendResponse( $results );
    }



    public function weekly(){
        $results = auth()->user()->expenses()
            ->select(
                DB::raw( 'sum(price) as sum' ),
                DB::raw("CONCAT(YEAR(date), '/', WEEK(date)) as date ")
            )
            ->orderBy( 'date' )
            ->groupBy(DB::raw("CONCAT(YEAR(date), '/', WEEK(date))") )
            ->get();

        return $this->sendResponse( $results );
    }
}
