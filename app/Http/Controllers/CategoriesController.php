<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use PHPUnit\Exception;

class CategoriesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index( Request $request )
    {
        $categories = Category::where( function ( $q ) use ( $request ) {

            $q->when( $request->id, function ( $q, $id ) {
                return $q->where( 'id', $id );
            } );

            $q->when( $request->name, function ( $q, $name ) {
                return $q->where( 'name', $name );
            } );

        } )
            ->get();

        return $this->sendResponse(
            $categories
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CategoryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store( CategoryRequest $request )
    {
        $category = Category::create( $request->all() );
        return $this->sendResponse( $category );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show( $id )
    {

        $category = Category::find( $id );

        if( $category )
            return $this->sendResponse( $category );
        else
            return $this->sendError( 'Not found' );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update( Request $request, $id )
    {
        $category = Category::find( $id );

        if( $category ){
            $category->update( $request->all() );
            return $this->sendResponse( $category );
        }  else {
            return $this->sendError( 'Not found' );
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy( $id )
    {
        $category = Category::find( $id );

        if( $category )
            return $this->sendResponse( [] );
        else {
            return $this->sendError( 'Not found' );
        }
    }
}
