<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'id' => 1,
                'name' => 'dario',
                'email' => 'dario@test.dev',
                'password' => Hash::make( 'dario123' ),
                'amount' => 123456.21
            ]
        ]);
    }
}
