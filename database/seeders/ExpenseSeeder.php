<?php

namespace Database\Seeders;

use App\Models\Expense;
use Carbon\CarbonPeriod;
use Illuminate\Database\Seeder;

class ExpenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $period = CarbonPeriod::create( '2020-01-01', '2022-02-01' );

        foreach ( $period as $date ){
            Expense::insert([
                'category_id' => 1,
                'user_id' => 1,
                'date' => $date->toDateString(),
                'price' => rand( 1, 99999 ),
                'note' => 'opet trošak'
            ]);
        }
    }
}
