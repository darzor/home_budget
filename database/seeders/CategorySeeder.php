<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert([
            [
                'name' => 'food'
            ],
            [
                'name' => 'car'
            ],
            [
                'name' => 'accommodation'
            ],
            [
                'name' => 'gifts'
            ],
            [
                'name' => 'beer'
            ],
            [
                'name' => 'fuel'
            ],
            [
                'name' => 'candy'
            ]
        ]);
    }
}
